FROM arm32v6/alpine

COPY qemu-arm-static /usr/bin

RUN apk update \
 && apk upgrade \
 && apk add mpd \
 && rm -rf /var/cache/apk/*

COPY mpd.conf /etc/mpd.conf
VOLUME /var/lib/mpd
EXPOSE 6600

RUN rm /usr/bin/qemu-arm-static

CMD ["mpd", "--stdout", "--no-daemon"]

